#include <iostream>
#include "TimeSupport.h"
#include "ArrayList.h"

using namespace std;

struct Task {
    string name;
    int time;

    Task() {
        name = "";
        time = 0;
    }

    Task(string name, int time) {
        this->name = name;
        this->time = time;
    }
};

int main(int argc, char* argv[]) {
    // determine if program should print scheduling order
    bool silent = false;
    if (argc == 2 && stoi(argv[1]) == 1) {
        silent = true;
    }

    // create tasks container
    ArrayList<Task> tasks;

    int period;
    int taskCount;
    cin >> period >> taskCount;

    // create and append tasks to container
    for (int i = 1; i <= taskCount; i++) {
        string name;
        int time;
        cin >> name >> time;
        Task task(name, time);
        tasks.append(task);
    }

    // start algorithm timestamp
    timestamp start = current_time();

    // execute all tasks in arrival order
    while (!tasks.isEmpty()) {
        // remove first task from container
        Task task = tasks.removeFirst();
        string taskName = task.name;
        int taskTime = task.time;

        // append task to container if it was not completed
        int timeRemaining = taskTime - period;
        if (timeRemaining > 0) {
            Task temp(taskName, timeRemaining);
            tasks.append(temp);
        }

        // print task name if silent is false
        if (!silent) {
            cout << taskName;
            if (!tasks.isEmpty()) {
                cout << " : ";
            }
        }
    }
    timestamp end = current_time();
    
    // print newline if silent is false
    if (!silent) {
        cout << endl;
    }

    // print duration of algorithm
    int duration = time_diff(start, end);
    cout << "Duration: " << duration << " ms" << endl;

    return 0;
}