all:
	g++ main.cpp -I. -o app

.PHONY: test
test:
	g++ -g -I. test.cpp -o test
	clear && echo "Running all tests:" && ./test --output=color

clean:
	rm -f app test temp