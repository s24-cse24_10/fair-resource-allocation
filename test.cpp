#include <igloo/igloo.h>
#include <stdexcept>
#include <unistd.h>
#include "TimeSupport.h"

using namespace igloo;

std::string exec(std::string command) {
   char buffer[128];
   std::string result = "";

   // Open pipe to file
   FILE* pipe = popen((command + " 2>&1").c_str(), "r");
   if (!pipe) {
      return "popen failed!";
   }

   // read till end of process:
   while (!feof(pipe)) {

      // use buffer to read and add to result
      if (fgets(buffer, 128, pipe) != NULL)
         result += buffer;
   }

   pclose(pipe);
   result.erase(result.find_last_not_of(" \t\n\r\f\v") + 1);
   return result;
}

struct OutputParser{
    std::vector<std::string> result;

    OutputParser(std::string output){
        std::stringstream ss(output);
        std::string line;
        while (getline(ss, line)){
            if (line != ""){
                result.push_back(line);
            }
        }
    }
};

Context(TestSchedulingTasks){
    static void SetUpContext() {
        exec("rm -f temp");
        exec("g++ main.cpp -o temp");
    }

    static void TearDownContext() {
        system("rm -rf temp");
    }

    Spec(TestNoRellocation) {
        std::string raw = exec("echo 2 3 T1 1 T2 2 T3 1 | ./temp");

        std::string actual = OutputParser(raw).result[0];
        std::string expected = "T1 : T2 : T3";

        Assert::That(actual, Equals(expected));
    }

    Spec(TestOneRellocation) {
        std::string raw = exec("echo 4 3 T1 1 T2 5 T3 4 | ./temp");

        std::string actual = OutputParser(raw).result[0];
        std::string expected = "T1 : T2 : T3 : T2";

        Assert::That(actual, Equals(expected));
    }

    Spec(TestTwoRellocation) {
        std::string raw = exec("echo 5 3 T1 8 T2 5 T3 6 | ./temp");

        std::string actual = OutputParser(raw).result[0];
        std::string expected = "T1 : T2 : T3 : T1 : T3";

        Assert::That(actual, Equals(expected));
    }

    Spec(TestManyRellocation) {
        std::string raw = exec("echo 3 7 T1 4 T2 7 T3 9 T4 12 T5 18 T6 3 T7 6 | ./temp");

        std::string actual = OutputParser(raw).result[0];
        std::string expected = "T1 : T2 : T3 : T4 : T5 : T6 : T7 : T1 : T2 : T3 : T4 : T5 : T7 : T2 : T3 : T4 : T5 : T4 : T5 : T5 : T5";

        Assert::That(actual, Equals(expected));
    }

    Spec(TestDuration1000) {
        std::string runCommand = "echo 5 1000";
        for (int i = 0; i < 1000; i++) {
            runCommand += " T" + std::to_string(i) + " " + std::to_string(i);
        }
        runCommand += " | ./temp 1";

        timestamp start = current_time();
        std::string raw = exec(runCommand);
        timestamp end = current_time();

        int duration = time_diff(start, end);

        Assert::That(duration, IsLessThan(500));
    }
};


int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}